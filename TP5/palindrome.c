#include<stdio.h>
#include<stdlib.h>

// version utilisant des indices
int palindrome1(char *s) {
    int i,j,l; // ou start, end

    // positionne i et j au début et fin de s
    i = 0;
    // on fait avancer j jusqu'au zéro terminal
    for (j = 0; s[j] != '\0'; j++) ; 
    // on a la longueur, on divise par deux
    l = j/2 - 1; 
    // on se place sur le dernier caractère
    j--; 
    // on est prêt à tester la palindromicité de la chaîne !
    // TODO
    while ( i < l && s[i] == s[j]) {
	    i++;
	    j--;
    }
    return s[i] == s[j];

    // return ...;
}

// version utilisant des pointeurs
int palindrome2(char *s) {
    char *p, *q;

    // positionne p et q au début et fin de s
    p = s;
    // on fait avancer q jusqu'au zéro terminal
    for (q = s; *q != '\0'; q++);
    // on se place sur le dernier caractère
    q--;
    // on est prêt à tester la palindromicité de la chaîne !
    // TODO
    while (q - p > 2 && *p == *q) {
	    p++;
	    q--;
    }
    return *p == *q;
    // return ...;
}

int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("Usage: %s whatever\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (palindrome1(argv[1])) {
        printf("%s est un palindrome (1)\n", argv[1]);
    } else {
        printf("%s n'est pas un palindrome (1)\n", argv[1]);
    }

    if (palindrome2(argv[1])) {
        printf("%s est un palindrome (2)\n", argv[1]);
    } else {
        printf("%s n'est pas un palindrome (2)\n", argv[1]);
    }
    exit(EXIT_SUCCESS);
}

