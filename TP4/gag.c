#include<stdio.h>

int main() {

    int tab[] = { 42, 12, 24, 0, -1, 36 };
    int *p;

    p = tab;
    printf("%d\n", *p);

    for (int i = 0; i < 6; i++) {
        // *(i + p) et i[tab] c'est pareil que *(p+i) et tab[i]
        // DON'T DO THAT !!!!
        printf("%d %d %d %d\n", tab[i], *(p+i), *(i+p), i[tab]);
    }

    return 0;
}
