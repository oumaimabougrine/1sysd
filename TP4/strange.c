#include<stdio.h>
#include<stdlib.h>

int slen(char *s) {
    int l = 0;
    char *p;
    p = s;
    while (*p++) {
        l++;
    }
    return l;
}
int sequal(char *s1, char *s2) {
	char *p, *q;
	p = s1;
	q = s2;

	while (*p != '\0' && *q != '\0' && *p == *q) {
		p++;
		q++;
	}
	return *p == *q;
}

int is_upper(char *s) {
    int upper = -1;
    // on passe à faux dès qu'on trouve une minuscule
    // du coup une chaîne sans lettre du tout est 
    // considérée comme toute en majuscule, ça se
    // défend (différent de isupper() en Python)
    while (*s && upper) { // le && upper rend le break inutile
        if (*s >= 'a' && *s <= 'z') { // minuscule
            upper = 0;
            //break;
        } 
        s++;
    }
    return upper;
}
void supper(char *s) {
	while (*s) {
		if (*s >= 'a' && *s <= 'z') {
			*s = *s - 32;
		}
		s++;
	}
}
char *scopy(char *s) {
    char *new, *p;
    new = malloc((slen(s) + 1)*sizeof(char)); // NULL term. str !!!!
    p = new;
    while (*s) {
        *p++ = *s++;
    }
    // ici p est arrivé à la fin
    *p = '\0'; // piège : oublie l'ajout du zéro terminal
               // on est pas sûr que l'espace alloué par malloc
               // est initialisé à 0000....
    return new;
}


// TODO: cf. transparents du cours.

int main() {
    char s1[] = "Hello World!";
    char s2[] = "Bonjour";
    char s3[] = "Bonjour";
    char s4[] = "Bonsoir";
    char s5[] = "Bon";
    char *s6;

    printf("Longeur de \"%s\" : %d\n", s1, slen(s1));
    
    if (sequal(s1, s2)) {
        printf("s1 et s2 sont égales\n");
    } else {
        printf("s1 et s2 sont différentes\n");
    }
    if (sequal(s2, s3)) {
        printf("s2 et s3 sont égales\n");
    } else {
        printf("s2 et s3 sont différentes\n");
    }
    if (sequal(s2, s4)) {
        printf("s2 et s4 sont égales\n");
    } else {
        printf("s2 et s4 sont différentes\n");
    }
    if (sequal(s2, s5)) {
        printf("s2 et s5 sont égales\n");
    } else {
        printf("s2 et s5 sont différentes\n");
    }

    supper(s1);
    printf("%s\n", s1);

    s6 = scopy(s1);
    printf("%s\n", s6);

    return 0;
}

