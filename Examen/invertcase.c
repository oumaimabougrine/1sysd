#include<stdio.h>
#include<stdlib.h>

void invertcase(char *s) {
	
    while (*s) { 
        if (*s >= 'a' && *s <= 'z') {
            *s -= 32;
        }else if (*s >= 'A' && *s <= 'Z') {
            *s += 32;
        }

        s++;
    }
    printf("%s\n", s);
}
int main(int argc, char *argv[]) {
  char *s;
  s = argv[1];
  if (argc != 2) {
    printf("Usage: ./invertcase val1 \n");
    exit(EXIT_FAILURE);
  }

  invertcase(s);
  printf("resultat %s\n :", s);

    exit(EXIT_SUCCESS);
}


