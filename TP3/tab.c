#include<stdio.h>
#include<stdlib.h>

#define MAXSIZE 5
int somme(int t[], int size) {
	int sum = 0;
	for (int i = 0; i < size; i++) {
		sum += t[i];
	}
	return sum;
}
int min(int t[], int size) {
    int m = t[0];
    for (int i = 1; i < size; i++) {
        if (t[i] < m) {
            m = t[i];
        }
    }
    return m;
}
int max(int t[], int size) {
    int m = t[0];
    for (int i = 1; i < size; i++) {
        if (t[i] > m) {
            m = t[i];
        }
    }
    return m;
}

int main() {
    int tab[MAXSIZE];
    int n;

    printf("Nombre d'entiers à saisir : ");
    scanf("%d", &n);
    if (n > MAXSIZE || n < 1) {
        printf("Maximum %d. Exiting.\n", MAXSIZE);
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < n; i++) {
        printf("Valeur %d : ", i);
        scanf("%d", &tab[i]);
    }
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");
    printf("la somme est : %d\n", somme(tab, n));
    printf("Max : %d Min : %d\n", max(tab, n), min(tab, n));
    return 0;
}

